# Run a site audit on site.asqatasun.ovh

## -----------------------------------------------------------------------------
## Create a contract for admin user dedicated to site.asqatasun.ovh
PUT {{proto}}://{{host}}:{{port}}{{path}}/contract
{
  "userId": 1,
  "label": "https://site.asqatasun.ovh/",
  "beginDate": "{{now}}",
  "endDate": "{{later}}",
  "functionalities": [
    "PAGES",
    "SCENARIO",
    "DOMAIN",
    "UPLOAD"
  ],
  "options": {
    "DOMAIN": "https://site.asqatasun.ovh/"
  },
  "referentials": [
    "RGAA_4_0"
  ]
}

HTTP 201
[Captures]
id_contract: body

## -----------------------------------------------------------------------------
## Verify contract is actually for site.asqatasun.ovh
GET {{proto}}://{{host}}:{{port}}{{path}}/contract/{{id_contract}}

HTTP 200
[Asserts]
jsonpath "$.optionElements.DOMAIN" == "https://site.asqatasun.ovh/"

## -----------------------------------------------------------------------------
## Run a site-audit
POST {{proto}}://{{host}}:{{port}}{{path}}/audit/site/run
{
  "url": "https://site.asqatasun.ovh/",
  "referential": "RGAA_4_0",
  "level": "AA",
  "depth": 10,
  "maxPages": 100,
  "maxDuration": 3600,
  "exclusionRegexp": "string",
  "inclusionRegexp": "string",
  "robotsTxtActivation": true,
  "contractId": {{id_contract}},
  "tags": [
    "API-testing-with-Hurl"
  ]
}

HTTP 201
[Captures]
id_audit: body

## -----------------------------------------------------------------------------
## Get status of the audit
GET {{proto}}://{{host}}:{{port}}{{path}}/audit/{{id_audit}}
[Options]
retry: 50
retry-interval: 1000

HTTP 200
[Asserts]
jsonpath "$.status" == "COMPLETED"
jsonpath "$.subject.type" == "GROUP_OF_PAGE"
jsonpath "$.subject.url" == "https://site.asqatasun.ovh"
jsonpath "$.subject.nbOfPages" == 3

## -----------------------------------------------------------------------------
## Delete contract
DELETE {{proto}}://{{host}}:{{port}}{{path}}/contract/{{id_audit}}

HTTP 200
