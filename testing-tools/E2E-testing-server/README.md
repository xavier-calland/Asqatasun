# API Testing with Hurl

## Pre-requisites

See https://hurl.dev/docs/installation.html

## How to run

```shell
cd server/asqatasun-server/src/test/Hurl/

export HURL_now=$(date '+%Y-%m-%dT%H:%M:%S')
export HURL_later=$(date '+%Y-%m-%dT%H:%M:%S' -d"+1year")
hurl \
  --variables-file hurl-options.env \
  --user admin@asqatasun.org:myAsqaPassword \
  --verbose \
  --report-html ./Report/ \
  --test Testsuite/*
```

## Resources

- https://hurl.dev
- https://about.gitlab.com/blog/2022/12/14/how-to-continously-test-web-apps-apis-with-hurl-and-gitlab-ci-cd/

