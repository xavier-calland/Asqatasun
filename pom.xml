<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE project>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.asqatasun</groupId>
    <artifactId>asqatasun</artifactId>
    <packaging>pom</packaging>
    <version>6.0.0-SNAPSHOT</version>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.2.1</version>
        <relativePath/>
    </parent>
    <name>asqatasun</name>
    <url>https://asqatasun.org/</url>
    <description>
        Asqatasun is an opensource website analyzer, used for web accessibility (a11y).
    </description>
    <organization>
        <name>Asqatasun.org</name>
        <url>https://asqatasun.org</url>
    </organization>
    <licenses>
        <license>
            <name>GNU Affero General Public License v3.0</name>
            <url>https://www.gnu.org/licenses/agpl-3.0.txt</url>
            <distribution>manual</distribution>
        </license>
    </licenses>
    <scm>
        <connection>scm:git:https://gitlab.com/asqatasun/Asqatasun.git</connection>
        <url>https://gitlab.com/asqatasun/Asqatasun</url>
        <developerConnection>scm:git:git@gitlab.com:asqatasun/Asqatasun.git</developerConnection>
        <tag>HEAD</tag>
    </scm>
    <issueManagement>
        <system>gitlab</system>
        <url>https://gitlab.com/asqatasun/Asqatasun/-/issues</url>
    </issueManagement>
    <ciManagement>
        <system>gitlab-ci</system>
        <url>https://gitlab.com/asqatasun/Asqatasun/-/pipelines</url>
    </ciManagement>

    <modules>
        <module>engine</module>
        <module>testing-tools</module>
        <module>rules</module>
        <module>web-app</module>
        <module>server</module>
    </modules>
    <profiles>
        <profile>
            <id>dev</id>
            <modules>
                <module>engine</module>
                <module>rules</module>
                <module>web-app</module>
            </modules>
        </profile>
    </profiles>
    <properties>
        <commitUrlPrefix>https://gitlab.com/asqatasun/Asqatasun/-/commit/</commitUrlPrefix>

        <!-- File Encoding -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>

        <!-- JDK + Kotlin versions -->
        <java.version>17</java.version>
        <kotlin.version>1.9.10</kotlin.version>

        <buildnumber-maven-plugin.version>1.4</buildnumber-maven-plugin.version>
        <dependency-check-maven.version>8.4.0</dependency-check-maven.version>
        <jacoco-maven-plugin.version>0.8.7</jacoco-maven-plugin.version>
        <markdown-page-generator-plugin.version>2.1.0</markdown-page-generator-plugin.version>
        <maven-checkstyle-plugin.version>3.0.0</maven-checkstyle-plugin.version>
        <maven-jxr-plugin.version>2.5</maven-jxr-plugin.version>
        <maven-project-info-reports-plugin.version>3.4.1</maven-project-info-reports-plugin.version>

        <apache-collection-utils.version>4.4</apache-collection-utils.version>
        <apache-http-client.version>4.5.14</apache-http-client.version>
        <commons-configuration.version>1.10</commons-configuration.version>
        <commons-csv.version>1.10.0</commons-csv.version>
        <commons-io.version>2.13.0</commons-io.version>
        <commons-net.version>3.9.0</commons-net.version>
        <commons-validator.version>1.7</commons-validator.version>
        <dbunit.version>2.7.3</dbunit.version>
        <dom4j.version>2.1.4</dom4j.version>
        <easymock.version>5.2.0</easymock.version>
        <heilbronn-crawler4j.version>5.0.2</heilbronn-crawler4j.version>
        <html-cleaner.version>2.29</html-cleaner.version>
        <jaxb-api.version>2.4.0-b180830.0359</jaxb-api.version>
        <jsoup.version>1.16.1</jsoup.version>
        <juniversalchardet.version>1.0.3</juniversalchardet.version>
        <lang-detect.version>1.1-20120112</lang-detect.version>
        <org-json.version>20231013</org-json.version>
        <ph-css.version>7.0.1</ph-css.version>
        <selenese-runner-java.version>4.3.0</selenese-runner-java.version>
        <spring-boot-starter.version>3.2.1</spring-boot-starter.version>
        <springdoc-openapi.version>1.7.0</springdoc-openapi.version>
        <springdoc-openapi-starter-webmvc-ui.version>2.2.0</springdoc-openapi-starter-webmvc-ui.version>
        <swagger-core-v3.version>2.2.15</swagger-core-v3.version>
        <tika-lang-detect.version>2.9.0</tika-lang-detect.version>
        <xstream.version>1.4.20</xstream.version>

        <!--        spring boot Overridden version due to detected vulnerability -->
        <hsqldb.version>2.7.2</hsqldb.version>
        <mysql.version>8.2.0</mysql.version>
        <snakeyaml.version>2.2</snakeyaml.version>
        <!-- WARNING force jaxb-impl version needed by selenese-runner on runtime to keep javax.xml.bind in classpath-->
        <glassfish-jaxb.version>2.3.8</glassfish-jaxb.version>

    </properties>

    <repositories>
        <repository>
            <url>https://repo1.maven.org/maven2/</url>
            <id>central</id>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>org.jetbrains.kotlin</groupId>
                        <artifactId>kotlin-maven-allopen</artifactId>
                        <version>${kotlin.version}</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <compilerPlugins>
                        <plugin>spring</plugin>
                    </compilerPlugins>
                </configuration>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
                            </sourceDirs>
                        </configuration>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>${project.basedir}/src/test/kotlin</sourceDir>
                            </sourceDirs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <argLine>
                        --add-opens java.base/java.util=ALL-UNNAMED
                    </argLine>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
            </plugin>
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <version>${maven-clean-plugin.version}</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>src/main/resources</directory>
                            <includes>
                                <include>**/hsql*</include>
                            </includes>
                            <followSymlinks>false</followSymlinks>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>${maven-jar-plugin.version}</version>
                <configuration>
                    <archive> <!-- - adds some information to the MANIFEST.MF files
                                   - uses build-helper-maven-plugin + buildnumber-maven-plugin
                                   - same as maven-war-plugin -->
                        <!--<manifest>
                                <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            </manifest>   -->
                        <manifestEntries>
                            <Project>${project.name}</Project>
                            <Built-By>${project.organization.name}</Built-By>
                            <!-- overwriting default value (user name) -->
                        </manifestEntries>
                        <manifestSections>
                            <manifestSection>
                                <name>Build</name>
                                <manifestEntries>
                                    <Build-Release>${project.version}</Build-Release>
                                    <Build-Timestamp>${timestamp}</Build-Timestamp>
                                    <Build-Id>${project.version}.${buildNumber}.${timestamp}</Build-Id>
                                </manifestEntries>
                            </manifestSection>
                            <manifestSection>
                                <name>Git</name>
                                <manifestEntries>
                                    <git-url>${commitUrlPrefix}${buildNumber}</git-url>
                                    <git-commit>${buildNumber}</git-commit>
                                </manifestEntries>
                            </manifestSection>
                            <manifestSection>
                                <name>Build-Information</name>
                                <manifestEntries>
                                    <Java-Version>${java.version}</Java-Version>
                                    <Os-Name>${os.name}</Os-Name>
                                    <Os-Arch>${os.arch}</Os-Arch>
                                </manifestEntries>
                            </manifestSection>
                        </manifestSections>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>${maven-war-plugin.version}</version>
                <configuration>
                    <archive> <!-- - adds some information to the MANIFEST.MF files
                                   - uses build-helper-maven-plugin + buildnumber-maven-plugin
                                   - same as maven-jar-plugin -->
                        <!--<manifest>
                                <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            </manifest>   -->
                        <manifestEntries>
                            <Project>${project.name}</Project>
                            <Built-By>${project.organization.name}</Built-By>
                            <!-- overwriting default value (user name) -->
                        </manifestEntries>
                        <manifestSections>
                            <manifestSection>
                                <name>Build</name>
                                <manifestEntries>
                                    <Build-Release>${project.version}</Build-Release>
                                    <Build-Timestamp>${timestamp}</Build-Timestamp>
                                    <Build-Id>${project.version}.${buildNumber}.${timestamp}</Build-Id>
                                </manifestEntries>
                            </manifestSection>
                            <manifestSection>
                                <name>Git</name>
                                <manifestEntries>
                                    <git-url>${commitUrlPrefix}${buildNumber}</git-url>
                                    <git-commit>${buildNumber}</git-commit>
                                </manifestEntries>
                            </manifestSection>
                            <manifestSection>
                                <name>Build-Information</name>
                                <manifestEntries>
                                    <Java-Version>${java.version}</Java-Version>
                                    <Os-Name>${os.name}</Os-Name>
                                    <Os-Arch>${os.arch}</Os-Arch>
                                </manifestEntries>
                            </manifestSection>
                        </manifestSections>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <!-- used by maven-jar-plugin
                                 maven-war-plugin

                         https://www.mojohaus.org/build-helper-maven-plugin
                         https://github.com/mojohaus/build-helper-maven-plugin  -->
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>${build-helper-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>timestamp-property</id>
                        <goals>
                            <goal>timestamp-property</goal>
                        </goals>
                        <phase>validate</phase>
                        <configuration>
                            <name>current.date</name>
                            <pattern>yyyy-MM-dd'T'HH:mm:ssZ</pattern>
                            <timeZone>UTC</timeZone>
                            <locale>en,US</locale>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- used by maven-jar-plugin
                                 maven-war-plugin

                         https://www.mojohaus.org/buildnumber-maven-plugin
                         https://github.com/mojohaus/buildnumber-maven-plugin  -->
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>${buildnumber-maven-plugin.version}</version>
                <configuration>
                    <!-- <timestampFormat>{0,date,yyyy-MM-dd-HH-mm-ss}</timestampFormat> -->
                    <shortRevisionLength>7</shortRevisionLength>
                    <revisionOnScmFailure>no-commit-ID</revisionOnScmFailure>
                    <!-- Provide this ${buildNumber} value if .git directory is missing-->
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!--
               Generate standalone Javadocs
               https://maven.apache.org/plugins/maven-javadoc-plugin/
               https://maven.apache.org/plugins/maven-javadoc-plugin/usage.html
               https://maven.apache.org/plugins/maven-javadoc-plugin/javadoc-mojo.html
               JavaDoc: target/site/apidocs
                        target/site/testapidocs
               Usage:   mvn javadoc:javadoc
                        mvn javadoc:aggregate
                        mvn javadoc:test-javadoc
                        mvn javadoc:test-aggregate           -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven-javadoc-plugin.version}</version>
                <configuration>
                    <show>private</show>
                    <author>false</author>
                    <linksource>true
                    </linksource> <!-- Creates an HTML version of each source file (with line numbers) -->
                    <doctitle>-- Javadocs for ${project.name} ${project.version}</doctitle>
                    <windowtitle>Javadocs for ${project.name} ${project.version}</windowtitle>
                    <header>-- ${project.name} v${project.version}</header>
                    <footer>${project.name} v${project.version} --</footer>
                    <bottom>
                        <![CDATA[
                            &#169; 2008&#x2013;{currentYear}
                            <a href="https://asqatasun.org">Asqatasun.org<a>
                        ]]>
                    </bottom>
                </configuration>
            </plugin>
            <!--
                OWASP Dependency-Check
                https://www.owasp.org/index.php/OWASP_Dependency_Check
                http://jeremylong.github.io/DependencyCheck/
                http://jeremylong.github.io/DependencyCheck/dependency-check-maven/
                report: target/dependency-check-report.html
                usage:  ./mvnw dependency-check:check
                        ./mvnw dependency-check:aggregate
                        ./mvnw dependency-check:help -Ddetail=true -Dgoal=aggregate       -->
            <plugin>
                <groupId>org.owasp</groupId>
                <artifactId>dependency-check-maven</artifactId>
                <version>${dependency-check-maven.version}</version>
                <configuration>
                    <!-- Disable dotNet content -->
                    <assemblyAnalyzerEnabled>false</assemblyAnalyzerEnabled>
                    <nugetconfAnalyzerEnabled>false</nugetconfAnalyzerEnabled>
                    <nuspecAnalyzerEnabled>false</nuspecAnalyzerEnabled>
                    <autoconfAnalyzerEnabled>false</autoconfAnalyzerEnabled>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!--
                Jacoco - Code coverage
                http://www.jacoco.org/jacoco/
                https://github.com/jacoco/jacoco/
                report: target/site/jacoco
                usage:  mvn clean install                     -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!--
                Generate HTML files (README, CHANGELOG, ...)
                in target/html/ directory  (used in tar.gz)
                https://github.com/walokra/markdown-page-generator-plugin
                https://github.com/sirthias/pegdown                         -->
            <plugin>
                <groupId>com.ruleoftech</groupId>
                <artifactId>markdown-page-generator-plugin</artifactId>
                <version>${markdown-page-generator-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>process-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <inputDirectory>${project.basedir}</inputDirectory>
                    <outputDirectory>${project.build.directory}/html/</outputDirectory>
                    <inputEncoding>UTF-8</inputEncoding>
                    <outputEncoding>UTF-8</outputEncoding>
                    <pegdownExtensions>TABLES,FENCED_CODE_BLOCKS</pegdownExtensions>
                    <!-- <pegdownExtensions>TABLES,FENCED_CODE_BLOCKS,AUTOLINKS, ANCHORLINKS, EXTANCHORLINKS</pegdownExtensions>  -->
                    <!-- <transformRelativeMarkdownLinks>true</transformRelativeMarkdownLinks> -->
                    <headerHtmlFile>${project.basedir}/testing-tools/resources/html_template/header.html
                    </headerHtmlFile>
                    <footerHtmlFile>${project.basedir}/testing-tools/resources/html_template/footer.html
                    </footerHtmlFile>
                </configuration>
            </plugin>
            <!-- Define minimal version of Maven to use -->
            <!--            <plugin>-->
            <!--                <groupId>org.apache.maven.plugins</groupId>-->
            <!--                <artifactId>maven-enforcer-plugin</artifactId>-->
            <!--                <version>3.0.0-M3</version>-->
            <!--                <executions>-->
            <!--                    <execution>-->
            <!--                        <id>enforce-maven</id>-->
            <!--                        <goals>-->
            <!--                            <goal>enforce</goal>-->
            <!--                        </goals>-->
            <!--                        <configuration>-->
            <!--                            <rules>-->
            <!--                                <requireMavenVersion>-->
            <!--                                    <version>3.6</version>-->
            <!--                                </requireMavenVersion>-->
            <!--                            </rules>-->
            <!--                        </configuration>-->
            <!--                    </execution>-->
            <!--                </executions>-->
            <!--            </plugin>-->
        </plugins>
    </build>

</project>
