/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.asqatasun.ruleimplementation;

import org.asqatasun.rules.elementchecker.doctype.DoctypeHtml5Checker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author jkowalczyk
 */
public class AbstractPageRuleWithDoctypeHtml5CheckerImplementationTest {

    @BeforeEach
    protected void setUp() throws Exception {
    }
    
    @AfterEach
    protected void tearDown() throws Exception {
    }

    @Test
    public void testElementCheckerIsDoctypeHtml5CheckerAndElementCheckerIsNull() {
        
        AbstractPageRuleWithDoctypeHtml5CheckerImplementation instance = 
                new MyConcreteClassToTest();
        
        assertTrue(instance.getElementChecker() instanceof DoctypeHtml5Checker);
        assertNull(instance.getElementSelector());
    }
    
    /**
     * Inner class that represents a concrete implementation of the 
     * AbstractPageRuleWithDoctypeHtml5CheckerImplementationTest abstract class
     */
    private class MyConcreteClassToTest extends AbstractPageRuleWithDoctypeHtml5CheckerImplementation {

        public MyConcreteClassToTest() {
            super();
        }
        
        
    }
    
}
