package org.asqatasun.webapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class EncoderConfig {

    //bean for md5 encryption
    @Bean
    public PasswordEncoder passwordEncoder() throws Exception {
        return new MessageDigestPasswordEncoder("MD5");
    }
}
