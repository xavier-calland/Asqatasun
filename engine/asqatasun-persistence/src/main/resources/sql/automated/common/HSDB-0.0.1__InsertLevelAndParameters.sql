--
-- Data for Name: level; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO level (id_level, cd_level, description, label, rank) VALUES (1, 'LEVEL_1', NULL, 'Level 1', 1);
INSERT INTO level (id_level, cd_level, description, label, rank) VALUES (2, 'LEVEL_2', NULL, 'Level 2', 2);
INSERT INTO level (id_level, cd_level, description, label, rank) VALUES (3, 'LEVEL_3', NULL, 'Level 3', 3);

-- Set Next ID Value to MAX ID


--
-- Data for Name: parameter_family; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO parameter_family (id_parameter_family, cd_parameter_family, description, long_label, short_label) VALUES (1, 'CRAWL', 'This parameter family handles all the parameters needed by the crawler component', 'crawl parameters', 'crawl params');
INSERT INTO parameter_family (id_parameter_family, cd_parameter_family, description, long_label, short_label) VALUES (2, 'GENERAL', 'This parameter family handles all the general parameters of the audit', 'general parameters', 'gen params');
INSERT INTO parameter_family (id_parameter_family, cd_parameter_family, description, long_label, short_label) VALUES (3, 'RULES', 'This parameter family handles all the parameters needed by the rules', 'rules parameters', 'rules params');
INSERT INTO parameter_family (id_parameter_family, cd_parameter_family, description, long_label, short_label) VALUES (4, 'Seo_TEST_WEIGHT_MANAGEMENT', 'This paramaters handles the test weight potentially overridden by users', 'test weight parameters', 'test weight params');

-- Set Next ID Value to MAX ID


--
-- Data for Name: parameter_element; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (1, 'MAX_DOCUMENTS', 1, 'max pages', 'Maximum number of downloaded pages');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (2, 'EXCLUSION_REGEXP', 1, 'exclusion regex', 'Regulard expression to exclude urls');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (3, 'DEPTH', 1, 'max depth', 'Maximum depth of the crawl');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (4, 'MAX_DURATION', 1, 'max duration', 'Maximum duration of the crawl');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (5, 'LEVEL', 2, 'Audit level', 'Audit level (includes the referential code)');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (6, 'DATA_TABLE_MARKER', 3, 'Data table marker', 'Data Table HTML marker (id or class)');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (7, 'PRESENTATION_TABLE_MARKER', 3, 'Presentation table marker', 'Presentation Table HTML marker (id or class)');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (8, 'DECORATIVE_IMAGE_MARKER', 3, 'Decorative image marker', 'Decorative image HTML marker (id or class)');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (9, 'INFORMATIVE_IMAGE_MARKER', 3, 'Informative image marker', 'Informative image HTML marker (id or class)');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (10, 'ALTERNATIVE_CONTRAST_MECHANISM', 3, 'Alternative Contrast Mechanism', 'The page embeds a mechanism that displays text with a correct ratio');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (11, 'CONSIDER_COOKIES', 1, 'consider cookies while crawling', 'consider cookies');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (12, 'INCLUSION_REGEXP', 1, 'inclusion regex', '');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (13, 'SCREEN_WIDTH', 2, 'screen width', '');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (14, 'SCREEN_HEIGHT', 2, 'screen height', '');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (15, 'COMPLEX_TABLE_MARKER', 3, 'Correspond to the attribute "id", "class" or "role" of the complex tables', 'Complex table marker');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (16, 'Seo-01011', 4, 'Seo-01011 weight', 'Weight of rule Seo-01011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (17, 'Seo-01012', 4, 'Seo-01011 weight', 'Weight of rule Seo-01011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (18, 'Seo-01013', 4, 'Seo-01011 weight', 'Weight of rule Seo-01011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (19, 'Seo-01021', 4, 'Seo-01021 weight', 'Weight of rule Seo-01021 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (20, 'Seo-01031', 4, 'Seo-01031 weight', 'Weight of rule Seo-01031 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (21, 'Seo-01041', 4, 'Seo-01041 weight', 'Weight of rule Seo-01041 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (22, 'Seo-01051', 4, 'Seo-01051 weight', 'Weight of rule Seo-01051 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (23, 'Seo-01061', 4, 'Seo-01061 weight', 'Weight of rule Seo-01061 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (24, 'Seo-01071', 4, 'Seo-01071 weight', 'Weight of rule Seo-01071 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (25, 'Seo-01081', 4, 'Seo-01081 weight', 'Weight of rule Seo-01081 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (26, 'Seo-02011', 4, 'Seo-02011 weight', 'Weight of rule Seo-02011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (27, 'Seo-02012', 4, 'Seo-02012 weight', 'Weight of rule Seo-02012 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (28, 'Seo-02013', 4, 'Seo-02013 weight', 'Weight of rule Seo-02013 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (29, 'Seo-03011', 4, 'Seo-01011 weight', 'Weight of rule Seo-03011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (30, 'Seo-03012', 4, 'Seo-03012 weight', 'Weight of rule Seo-03012 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (31, 'Seo-05011', 4, 'Seo-05011 weight', 'Weight of rule Seo-05011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (32, 'Seo-05012', 4, 'Seo-05012 weight', 'Weight of rule Seo-05012 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (33, 'Seo-05013', 4, 'Seo-05013 weight', 'Weight of rule Seo-05013 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (34, 'Seo-06011', 4, 'Seo-06011 weight', 'Weight of rule Seo-06011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (35, 'Seo-06021', 4, 'Seo-06021 weight', 'Weight of rule Seo-06021 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (36, 'Seo-06031', 4, 'Seo-06031 weight', 'Weight of rule Seo-06031 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (37, 'Seo-06041', 4, 'Seo-06041 weight', 'Weight of rule Seo-06041 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (38, 'Seo-06051', 4, 'Seo-06051 weight', 'Weight of rule Seo-06051 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (39, 'Seo-06052', 4, 'Seo-06052 weight', 'Weight of rule Seo-06052 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (40, 'Seo-07011', 4, 'Seo-07011 weight', 'Weight of rule Seo-07011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (41, 'Seo-07012', 4, 'Seo-07012 weight', 'Weight of rule Seo-07012 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (42, 'Seo-07013', 4, 'Seo-07013 weight', 'Weight of rule Seo-07012 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (43, 'Seo-07021', 4, 'Seo-07021 weight', 'Weight of rule Seo-07021 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (44, 'Seo-07051', 4, 'Seo-07051 weight', 'Weight of rule Seo-07051 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (45, 'Seo-07061', 4, 'Seo-07061 weight', 'Weight of rule Seo-07061 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (46, 'Seo-08011', 4, 'Seo-08011 weight', 'Weight of rule Seo-08011 overriden by user');
INSERT INTO parameter_element (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (47, 'ROBOTS_TXT_ACTIVATION', 1, 'robots txt', 'Consider robots.txt directives when crawling');



--
-- Data for Name: parameter; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (1, '50000', 1, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (2, '', 2, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (3, '20', 3, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (4, '0', 3, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (5, '604800', 4, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (6, '100', 1, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (7, '10000', 1, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (8, '20000', 1, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (9, '', 6, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (10, '', 7, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (11, '86400', 4, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (12, '', 8, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (13, '', 9, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (14, 'true', 10, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (15, 'false', 10, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (16, 'true', 11, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (17, 'false', 11, false);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (18, '', 12, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (19, '1920', 13, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (20, '1080', 14, true);
INSERT INTO parameter (id_parameter, parameter_value, id_parameter_element, is_default) VALUES (21, '', 15, true);


