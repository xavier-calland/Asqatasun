INSERT INTO PARAMETER_ELEMENT (id_parameter_element, cd_parameter_element, id_parameter_family, short_label, long_label) VALUES (48, 'ALLOW_URL_DUPLICATION_IN_AUDIT', 2, 'Allow url duplication in audit', 'When launching scenario audit, same url may be encountered several times, this parameter allows this behaviour when needed');
INSERT INTO PARAMETER (parameter_value, id_parameter_element, is_default) VALUES (false, 48, true);
INSERT INTO PARAMETER (parameter_value, id_parameter_element, is_default) VALUES (true, 48, false);
